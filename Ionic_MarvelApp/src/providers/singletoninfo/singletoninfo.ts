import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class SingletoninfoProvider {

  private heroesList:   any;
  private comicsList:   any;
  private creatorsList: any;

  private favHeroesList:    any;
  private favComicsList:    any;
  private favCreatorsList:  any;

  constructor(public http: HttpClient) {
    this.heroesList   = null;
    this.comicsList   = null;
    this.creatorsList = null;

    this.favHeroesList    = null;
    this.favComicsList    = null;
    this.favCreatorsList  = null;
  }

  setHeroesList   (list: any) { this.heroesList   = list;}
  setComicsList   (list: any) { this.comicsList   = list;}
  setCreatorsList (list: any) { this.creatorsList = list;}

  setFavHeroesList   (list: any) { this.favHeroesList   = list;}
  setFavComicsList   (list: any) { this.favComicsList   = list;}
  setFavCreatorsList (list: any) { this.favCreatorsList = list;}

  getHeroresList()  { return this.heroesList;}
  getComicsList()   { return this.comicsList;}
  getCreatorsList() { return this.creatorsList;}

  getFavHeroresList()  { return this.favHeroesList;}
  getFavComicsList()   { return this.favComicsList;}
  getFavCreatorsList() { return this.favCreatorsList;}

  
}
