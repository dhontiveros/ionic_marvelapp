import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;
import { UserInfo } from '../../app/models/userinfo';

@Injectable()
export class AuthServiceProvider {

  currentUser: UserInfo;
  
  constructor(public afAuth: AngularFireAuth) {
    
  }

  doLogin(email: string, password: string): any {
    console.log("Haciendo login contra FIREBASE...")
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.currentUser = new UserInfo(
          '0', 
          user.user.email, 
          user.user.displayName);
      });
  }


  register(email: string, password: string): any {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((newUser) => {
        console.log("Nuevo usuario registrado");
        console.log(newUser);
      });
  }

  resetPassword(email: string): any {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  doLogout(): any {
    return this.afAuth.auth.signOut();
  }

  getUserInfo(){
    return this.currentUser;
  }

  setUserInfo(user: UserInfo){
    console.log("Recibiendo usuario nuevo...");
    console.log(user);
    this.currentUser = user;
  }

  

}
