import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserInfo } from '../../app/models/userinfo';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { map } from 'rxjs/operators';
import { AuthServiceProvider } from '../auth-service/auth-service';


@Injectable()
export class DaoFirebaseProvider {
  private dbPath = "/userProfile";
  
  usersList: AngularFireList<UserInfo> = null;

  /***************************************************************************************************************
   * 
   * @param http 
   * @param authProvider 
   * @param afDatabase 
   ***************************************************************************************************************/
  constructor(  public http: HttpClient,
                private authProvider: AuthServiceProvider,
                private afDatabase: AngularFireDatabase) 
  {
    this.usersList = this.afDatabase.list(this.dbPath);
  }


  /***************************************************************************************************************
   * 
   * @param user 
   ***************************************************************************************************************/
  registerUser(user: UserInfo){
    // Se emplea el método set en lugar del método push(), para poder asignar un valor al ID dentro de Firebase
    // con el fin de poder acceder luego al elemento individual correctamente
    return this.usersList.set(user.getId(), user);
  }

  /***************************************************************************************************************
   * 
   * @param emailValue 
   ***************************************************************************************************************/
  getUserInfoByEmail(emailValue: string){
    return this.afDatabase.list(this.dbPath, ref=>ref.orderByChild('email').equalTo(emailValue).limitToFirst(1)).valueChanges().pipe(
      map( (res: any) => res),
      map( (error: any) => error) 
    );
  }

  
  /***************************************************************************************************************
   * 
   * @param fav 
   * @param typeFav 
   ***************************************************************************************************************/
  registerFav(fav: any, typeFav: number){
    // Inserción del elemento en memoria
    let userInfo = this.authProvider.getUserInfo();
    userInfo.addFavItem(fav,typeFav);
    this.authProvider.setUserInfo(userInfo);

    return this.updateListInBD(userInfo, typeFav);
  }


  /***************************************************************************************************************
   * 
   * @param fav 
   * @param typeFav 
   ***************************************************************************************************************/
  deleteFav(fav: any, typeFav: number){
    // Eliminación del elemento en memoria
    let userInfo = this.authProvider.getUserInfo();
    userInfo.removeFavItem(fav,typeFav);
    this.authProvider.setUserInfo(userInfo);

    // Actualización de la lista en BD:
    return this.updateListInBD(userInfo,typeFav);
  }


  /***************************************************************************************************************
   * 
   ***************************************************************************************************************/
  getAllFavs(){
    return this.afDatabase.list(this.dbPath).valueChanges().pipe(
      map( (res: any) => res),
      map( (error: any) => error) 
    );
  }

  /***************************************************************************************************************
   * 
   * @param userInfo 
   * @param typeFav 
   ***************************************************************************************************************/
  updateListInBD(userInfo: UserInfo, typeFav: number)
  {
    // Actualización de la lista en BD:
    switch( typeFav )
    {
      // --------------------------------------------------------------------------------------
      case UserInfo.HERO: 
        return this.afDatabase.object(this.dbPath+"/"+userInfo.getId()).update({
          favHeroesList: userInfo.getFavList(typeFav)
        });
      // --------------------------------------------------------------------------------------
      case UserInfo.COMIC: 
        return this.afDatabase.object(this.dbPath+"/"+userInfo.getId()).update({
          favComicsList: userInfo.getFavList(typeFav)
        });
      // --------------------------------------------------------------------------------------
      case UserInfo.CREATOR: 
        return this.afDatabase.object(this.dbPath+"/"+userInfo.getId()).update({
          favCreatorsList: userInfo.getFavList(typeFav)
        });
      // --------------------------------------------------------------------------------------
    }
  }

}
