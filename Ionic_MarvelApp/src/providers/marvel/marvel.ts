import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { MarvelConfig } from '../../app/models/marvelconfig';


@Injectable()
export class MarvelProvider {

  private marvelConfig: MarvelConfig;

  constructor(public http: HttpClient) {
    console.log("MARVEL_PROVIDER");
    this.marvelConfig = new MarvelConfig();
  }

  
  getListHeroes(){
    return this.http.get( this.marvelConfig.getListHeroes() )
      .pipe( 
        map ( (res : any) => res),
        map( (error: any) => error )
      );
  }
  

  getDescriptionHeroe(id: number){
    return this.http.get( this.marvelConfig.getDescriptionHeroe(id) )
      .pipe( 
        map ( (res : any) => res),
        map( (error: any) => error )
      );
  }

  getListComics(){
    return this.http.get( this.marvelConfig.getListComics() )
      .pipe( 
        map ( (res : any) => res),
        map( (error: any) => error )
      );
  }

  getDescriptionComic(id: number){
    return this.http.get( this.marvelConfig.getDescriptionComic(id) )
      .pipe( 
        map ( (res : any) => res),
        map( (error: any) => error )
      );
  }

  getListsCreators(){
    return this.http.get( this.marvelConfig.getListCreators() )
      .pipe( 
        map ( (res : any) => res),
        map( (error: any) => error )
      );
  }

  getDescriptionCreator(id: number){
    return this.http.get( this.marvelConfig.getDescriptionCreator(id) )
      .pipe( 
        map ( (res : any) => res),
        map( (error: any) => error )
      );
  }
}
