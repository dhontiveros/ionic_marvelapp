import { Component, ViewChild } from '@angular/core';
import { NavController, Nav, MenuController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UserInfo } from '../../app/models/userinfo';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Nav) nav: Nav;
  pages: Array<{title: string, component: any}>;
  rootPage = 'HeroesPage';

  public userInfo: UserInfo;

  constructor(  public navCtrl: NavController, 
                private authService: AuthServiceProvider,
                private menuCtrl: MenuController) 
  {
    this.pages = [
      { title: 'Heroes',      component: 'HeroesPage' },
      { title: 'Comics',      component: 'ComicsPage' },
      { title: 'Creators',   component: 'CreatorsPage' },
      { title: 'Favorites',   component: 'FavoritesPage' },
      { title: 'About',   component: 'AboutPage' },
    ];

    this.userInfo = authService.getUserInfo();
    console.log("USUARIO DESDE <HOME>");
    console.log(this.userInfo);
    this.menuCtrl.enable(true);
  }

  openPage(page) {
    this.nav.setRoot(page.component);
    
  }

  doLogout(){
    this.authService.doLogout();
    this.menuCtrl.close();
    this.navCtrl.setRoot(LoginPage);
  }
}
