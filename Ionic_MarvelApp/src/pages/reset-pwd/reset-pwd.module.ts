import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResetpwdPage } from './reset-pwd';

@NgModule({
  declarations: [
    ResetpwdPage,
  ],
  imports: [
    IonicPageModule.forChild(ResetpwdPage),
  ],
})
export class ResetpwdPageModule {}
