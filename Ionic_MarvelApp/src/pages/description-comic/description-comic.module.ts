import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionComicPage } from './description-comic';

@NgModule({
  declarations: [
    DescriptionComicPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionComicPage),
  ],
})
export class DescriptionComicPageModule {}
