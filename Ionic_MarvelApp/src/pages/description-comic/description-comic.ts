import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Comic } from '../../app/models/comic';
import { MarvelProvider } from '../../providers/marvel/marvel';
import { WarningAlert } from '../../components/warningalert';
import { DaoFirebaseProvider } from '../../providers/dao-firebase/dao-firebase';
import { UserInfo } from '../../app/models/userinfo';

@IonicPage()
@Component({
  selector: 'page-description-comic',
  templateUrl: 'description-comic.html',
})
export class DescriptionComicPage {

  id: number;
  public isFav: boolean;
  comic: Comic = new Comic();
  warning: WarningAlert;

  constructor(  public navCtrl:         NavController, 
                public navParams:       NavParams, 
                private alertCtrl:      AlertController,
                private toastCtrl:      ToastController,
                private marvelProvider: MarvelProvider,
                private daoFirebase:    DaoFirebaseProvider) 
  {
    this.id       = navParams.get('id');
    this.isFav    = navParams.get('isFav');
    this.warning  = new WarningAlert();

    this.marvelProvider.getDescriptionComic(this.id)
    .subscribe( res => {      
      this.comic.id           = this.id;
      this.comic.title        = res.data.results[0].title;
      this.comic.thumb        = res.data.results[0].thumbnail.path+"."+res.data.results[0].thumbnail.extension;
      this.comic.pages        = res.data.results[0].pageCount;

      if( res.data.results[0].prices.length>0 ){
        this.comic.printPrice   = res.data.results[0].prices[0].price;
      }

      if( res.data.results[0].textObjects.length>0 ){
        this.comic.description  = res.data.results[0].textObjects[0].text;
      }
    });
  }

  addDeleteFav(){
    if( this.isFav ){
      this.daoFirebase.deleteFav(this.comic, UserInfo.COMIC).then( 
        response=>  { this.showOKResponseAddDeleteFav();          },
        error=>     { this.warning.errorFavAlert(this.alertCtrl); } 
      );
    }
    else{
      this.daoFirebase.registerFav(this.comic, UserInfo.COMIC).then( 
        response=>  { this.showOKResponseAddDeleteFav();          },
        error=>     { this.warning.errorFavAlert(this.alertCtrl); } 
      );
    }
  }

  showOKResponseAddDeleteFav(){
    this.isFav = !this.isFav; 
    this.warning.addDeleteElement(this.toastCtrl, this.isFav, "cómic");
  }
}