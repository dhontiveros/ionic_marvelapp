import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionCreatorPage } from './description-creator';

@NgModule({
  declarations: [
    DescriptionCreatorPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionCreatorPage),
  ],
})
export class DescriptionCreatorPageModule {}
