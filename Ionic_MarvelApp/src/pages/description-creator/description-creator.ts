import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Creator } from '../../app/models/creator';
import { MarvelProvider } from '../../providers/marvel/marvel';
import { WarningAlert } from '../../components/warningalert';
import { DaoFirebaseProvider } from '../../providers/dao-firebase/dao-firebase';
import { UserInfo } from '../../app/models/userinfo';

@IonicPage()
@Component({
  selector: 'page-description-creator',
  templateUrl: 'description-creator.html',
})
export class DescriptionCreatorPage {

  id: number;
  public isFav: boolean;
  creator: Creator = new Creator();
  warning: WarningAlert;

  constructor(  public navCtrl:         NavController, 
                public navParams:       NavParams,
                private toastCtrl:      ToastController,
                private alertCtrl:      AlertController,
                private daoFirebase:    DaoFirebaseProvider,
                private marvelProvider: MarvelProvider) 
  {
    this.id       = navParams.get('id');
    this.isFav    = navParams.get('isFav');
    this.warning  = new WarningAlert();

    this.marvelProvider.getDescriptionCreator(this.id)
    .subscribe( res => {

      this.creator.id           = this.id;
      this.creator.fullName     = res.data.results[0].fullName;
      this.creator.thumb        = res.data.results[0].thumbnail.path+"."+res.data.results[0].thumbnail.extension;
    });
  }


  addDeleteFav(){
    console.log("isFav = "+this.isFav);
    if( this.isFav ){
      console.log("isFav 2 = antes del dao");
      this.daoFirebase.deleteFav(this.creator, UserInfo.CREATOR).then( 
        response=>  { this.showOKResponseAddDeleteFav();          },
        error=>     { this.warning.errorFavAlert(this.alertCtrl); } 
      );
    }
    else{
      console.log("isFav 2 = antes del daoReg");
      this.daoFirebase.registerFav(this.creator, UserInfo.CREATOR).then( 
        response=>  { this.showOKResponseAddDeleteFav();          },
        error=>     { this.warning.errorFavAlert(this.alertCtrl); } 
      );
    }
  }

  showOKResponseAddDeleteFav(){
    this.isFav = !this.isFav; 
    this.warning.addDeleteElement(this.toastCtrl, this.isFav, "creador");
  }

}
