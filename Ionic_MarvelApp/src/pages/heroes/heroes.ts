import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, MenuController } from 'ionic-angular';
import { MarvelProvider } from '../../providers/marvel/marvel';
import { Hero } from '../../app/models/hero';
import { DescriptionHeroPage } from '../description-hero/description-hero';
import { SingletoninfoProvider } from '../../providers/singletoninfo/singletoninfo';
import { WarningAlert } from '../../components/warningalert';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UserInfo } from '../../app/models/userinfo';


@IonicPage()
@Component({
  selector: 'page-heroes',
  templateUrl: 'heroes.html',
})
export class HeroesPage {

  public  heroes :          Hero[]; // any
  public  filteredHeroes:   Hero[]; // any

  public  loadedList:       boolean;
  private loading:          Loading;
  private warningAlert:     WarningAlert;

  constructor(  public  navCtrl:          NavController, 
                public  navParams:        NavParams,
                public  menuCtrl:         MenuController,
                private marvelProvider:   MarvelProvider,
                private sInfoProvider:    SingletoninfoProvider,
                private authProvider:     AuthServiceProvider,
                private alertController:  AlertController,
                private loadingCtrl:      LoadingController ) 
  {
    this.warningAlert = new WarningAlert();
    this.loadedList   = false;
    this.loading      = this.loadingCtrl.create({
      content: 'Obtaining heroes list. Please wait..'
    });
    this.getAllHeroes();
  }

  
  /*********************************************************************************************************
   * 
   * @param marvelProvider
   *********************************************************************************************************/
  getAllHeroes(){
    if( this.sInfoProvider.getHeroresList() ){
      this.loadInfoByCurrentValue( this.sInfoProvider.getHeroresList() );
      return;
    } 

    this.loading.present();
    console.log('realizando llamada desde HeroesPage...');
    this.marvelProvider.getListHeroes()
      .subscribe( 
        res => {
          console.log( res );
          this.loading.dismiss();
          this.loadInfoByCurrentValue( res.data.results );
          this.sInfoProvider.setHeroesList( res.data.results );        
        },
        error => {
          this.loading.dismiss();
          this.warningAlert.errorResponseAlert(this.alertController);
        } 
      );
  }


  /*********************************************************************************************************
   * 
   * @param dataValue 
   *********************************************************************************************************/
  loadInfoByCurrentValue(dataValue: any){
      this.heroes         = dataValue;
      this.filteredHeroes = dataValue;
      this.loadedList     = true;
  }


  /*********************************************************************************************************
   * 
   * @param id 
   *********************************************************************************************************/
  getDescription(hero: Hero){
    this.navCtrl.push(DescriptionHeroPage,{
      id : hero.id,
      isFav: this.checkHeroIsFav(hero)
    });
  }

  /*********************************************************************************************************
   * 
   * @param id 
   *********************************************************************************************************/
  checkHeroIsFav(hero: Hero): boolean{
    let list = this.authProvider.getUserInfo().getFavList(UserInfo.HERO);
    if( list==-1 ) return false;
    return list.find(function(element) {
      return element.id === hero.id;
    });
  }


  /*********************************************************************************************************
   * 
   *********************************************************************************************************/
  initializeItems(){
    if( this.heroes ){
      this.filteredHeroes = [];
      this.filteredHeroes = this.heroes;
    }
  }
 

  /*********************************************************************************************************
   * 
   * @param ev 
   *********************************************************************************************************/
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;
    
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.filteredHeroes = this.filteredHeroes.filter((item) => {
        console.log(item);
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  openSideMenu(){
    this.menuCtrl.enable(true)
    this.menuCtrl.toggle(); 
  }
}
