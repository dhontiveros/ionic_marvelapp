import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { MarvelProvider } from '../../providers/marvel/marvel';
import { DescriptionComicPage } from '../description-comic/description-comic';
import { SingletoninfoProvider } from '../../providers/singletoninfo/singletoninfo';
import { WarningAlert } from '../../components/warningalert';
import { Comic } from '../../app/models/comic';
import { UserInfo } from '../../app/models/userinfo';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-comics',
  templateUrl: 'comics.html',
})
export class ComicsPage {

  public  comics :          any; // any
  public  filteredComics:   any; // any

  public  loadedList:       boolean;
  private loading:          Loading;
  private warningAlert:     WarningAlert;

  constructor(  public    navCtrl:          NavController, 
                public    navParams:        NavParams, 
                private   marvelProvider:   MarvelProvider,
                private   sInfoProvider:    SingletoninfoProvider,
                private   authProvider:     AuthServiceProvider,
                private   alertController:  AlertController,
                private   loadingCtrl:      LoadingController) 
  {
    this.warningAlert = new WarningAlert();
    this.loadedList   = false;
    this.loading      = this.loadingCtrl.create({
      content: 'Obtaining comics list. Please wait..'
    });
    this.getAllComics();
  }

  /*********************************************************************************************************
   * 
   *********************************************************************************************************/
  getAllComics(){
    if( this.sInfoProvider.getComicsList() ){
      this.loadInfoByCurrentValue( this.sInfoProvider.getComicsList() );
      return;
    }

    this.loading.present();
    console.log('realizando llamada desde ComicsPage...');
    this.marvelProvider.getListComics()
    .subscribe( 
      res => {
        console.log( res );
          this.loading.dismiss();
          this.loadInfoByCurrentValue( res.data.results );
          this.sInfoProvider.setComicsList( res.data.results );
      },
      error => {
        this.loading.dismiss();
        this.warningAlert.errorResponseAlert(this.alertController);
      }
    );
  }

  /*********************************************************************************************************
   * 
   * @param dataValue 
   *********************************************************************************************************/
  loadInfoByCurrentValue(dataValue: any){
    this.comics         = dataValue;
    this.filteredComics = dataValue;
    this.loadedList     = true;
  }

  /*********************************************************************************************************
   * 
   * @param id 
  *********************************************************************************************************/
  getDescription(comic: Comic){
    this.navCtrl.push(DescriptionComicPage,{
      id : comic.id,
      isFav: this.checkComicIsFav(comic)
    });
  }

  /*********************************************************************************************************
   * 
   * @param id 
   *********************************************************************************************************/
  checkComicIsFav(comic: Comic): boolean{
    let list = this.authProvider.getUserInfo().getFavList(UserInfo.COMIC);
    if( list==-1 ) return false;
    return list.find(function(element) {
      return element.id === comic.id;
    });
  }


  /*********************************************************************************************************
   * 
   *********************************************************************************************************/
  initializeItems(){
    if( this.comics ){
      this.filteredComics = [];
      this.filteredComics = this.comics;
    }
  }
 

  /*********************************************************************************************************
   * 
   * @param ev 
   *********************************************************************************************************/
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;
    
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.filteredComics = this.filteredComics.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
