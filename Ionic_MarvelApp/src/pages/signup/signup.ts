import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { DaoFirebaseProvider } from '../../providers/dao-firebase/dao-firebase';
import { UserInfo } from '../../app/models/userinfo';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  public registerForm;
  emailChanged:     boolean = false;
  passwordChanged:  boolean = false;
  fullnameChanged:  boolean = false;
  submitAttempt:    boolean = false;
  loading:          any;

  constructor(  public navCtrl:       NavController, 
                public authService:   AuthServiceProvider,
                public daoService:    DaoFirebaseProvider,
                public navParams:     NavParams, 
                public formBuilder:   FormBuilder,
                public alertCtrl:     AlertController, 
                public loadingCtrl:   LoadingController) 
  {
    this.registerForm = formBuilder.group({
      email:      ['', Validators.compose([Validators.required, Validators.email])],
      fullname:   ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      password:   ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  elementChanged(input){
    let field = input.ngControl.name;
    this[field + "Changed"] = true;
  }

  doRegister(){
    this.submitAttempt = true;

    if (!this.registerForm.valid){
      console.log(this.registerForm.value);
    } else {
      this.authService.register(this.registerForm.value.email, this.registerForm.value.password).then( authService => 
      {
        // -----------------------------------------------------------------------------------
        let userInfo = new UserInfo(new Date().getTime().toString(), this.registerForm.value.email, this.registerForm.value.fullname);
        this.daoService.registerUser(userInfo).then( response =>  
          {
            console.log("Register USER OK!");
            console.log(response);
            this.authService.setUserInfo(userInfo);
            this.navCtrl.popToRoot();
          }, 
          errorResponse => 
          {
            console.log("Register USER ERROR!");
            console.log(errorResponse);
          }
        );
        // -----------------------------------------------------------------------------------
        
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }

}