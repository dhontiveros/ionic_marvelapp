import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import { Creator } from '../../app/models/creator';
import { MarvelProvider } from '../../providers/marvel/marvel';
import { DescriptionCreatorPage } from '../description-creator/description-creator';
import { SingletoninfoProvider } from '../../providers/singletoninfo/singletoninfo';
import { WarningAlert } from '../../components/warningalert';
import { UserInfo } from '../../app/models/userinfo';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@IonicPage()
@Component({
  selector: 'page-creators',
  templateUrl: 'creators.html',
})
export class CreatorsPage {

  public  creators :          any; // any
  public  filteredCreators:   any; // any

  public  loadedList:         boolean;
  private loading:            Loading;
  private warningAlert:        WarningAlert;

  constructor(  public navCtrl:           NavController, 
                public navParams:         NavParams,
                private marvelProvider:   MarvelProvider,
                private authProvider:     AuthServiceProvider,
                private sInfoProvider:    SingletoninfoProvider,
                private alertController:  AlertController,
                private loadingCtrl:      LoadingController) 
  {
    this.warningAlert = new WarningAlert();
    this.loadedList   = false;
    this.loading      = this.loadingCtrl.create({
      content: 'Obtaining creators list. Please wait..'
    });
    this.getAllCreators();
  }


  getAllCreators(){
    if( this.sInfoProvider.getCreatorsList() ){
      this.loadInfoByCurrentValue( this.sInfoProvider.getCreatorsList() );
      return;
    } 

    this.loading.present();
    console.log('realizando llamada desde CreatorsPage...');
    this.marvelProvider.getListsCreators()
      .subscribe( 
        res => {
          console.log( res );
          this.loading.dismiss();
          this.loadInfoByCurrentValue( res.data.results );
          this.sInfoProvider.setCreatorsList( res.data.results );
        },
        erro => {
          this.loading.dismiss();
          this.warningAlert.errorResponseAlert(this.alertController);
        } 
      );
  }

  /*********************************************************************************************************
   * 
   * @param dataValue 
   *********************************************************************************************************/
  loadInfoByCurrentValue(dataValue: any){
    this.creators         = dataValue;
    this.filteredCreators = dataValue;
    this.loadedList       = true;
}


  /*********************************************************************************************************
   * 
   * @param id 
  *********************************************************************************************************/
  getDescription(creator: Creator){
    this.navCtrl.push(DescriptionCreatorPage,{
      id : creator.id,
      isFav: this.checkCreatorIsFav(creator)
    });
  }

  /*********************************************************************************************************
   * 
   * @param id 
   *********************************************************************************************************/
  checkCreatorIsFav(creator: Creator): boolean{
    let list = this.authProvider.getUserInfo().getFavList(UserInfo.CREATOR);
    if( list==-1 ) return false;
    return list.find(function(element) {
      return element.id === creator.id;
    });
  }

  /*********************************************************************************************************
   * 
   *********************************************************************************************************/
  initializeItems(){
    if( this.creators ){
      this.filteredCreators = [];
      this.filteredCreators = this.creators;
    }
  }

  /*********************************************************************************************************
   * 
   * @param ev 
   *********************************************************************************************************/
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;
    
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.filteredCreators = this.filteredCreators.filter((item) => {
        console.log(item);
        return (item.fullName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
