import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionHeroPage } from './description-hero';

@NgModule({
  declarations: [
    DescriptionHeroPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionHeroPage),
  ],
})
export class DescriptionHeroPageModule {}
