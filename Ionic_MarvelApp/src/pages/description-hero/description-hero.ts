import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { MarvelProvider } from '../../providers/marvel/marvel';
import { Hero } from '../../app/models/hero';
import { WarningAlert } from '../../components/warningalert';
import { DaoFirebaseProvider } from '../../providers/dao-firebase/dao-firebase';
import { UserInfo } from '../../app/models/userinfo';

@IonicPage()
@Component({
  selector: 'page-description-hero',
  templateUrl: 'description-hero.html',
})
export class DescriptionHeroPage {

  id: number;
  public isFav: boolean;
  hero: Hero = new Hero();
  warning: WarningAlert;

  constructor(  public navCtrl: NavController, 
                public navParams: NavParams,
                private alertCtrl: AlertController,
                private toastCtrl: ToastController,
                private marvelProvider: MarvelProvider,
                private daoFirebase: DaoFirebaseProvider) 
  {
    this.id       = navParams.get('id');
    this.isFav    = navParams.get('isFav');
    this.warning  = new WarningAlert();

    this.marvelProvider.getDescriptionHeroe(this.id)
    .subscribe( res => {
      this.hero.id          = this.id;
      this.hero.name        = res.data.results[0].name;
      this.hero.thumb       = res.data.results[0].thumbnail.path+"."+res.data.results[0].thumbnail.extension;
      this.hero.description = res.data.results[0].description;
    });
  }

  addDeleteFav(){
    if( this.isFav ){
      this.daoFirebase.deleteFav(this.hero, UserInfo.HERO).then( 
        response=>  { this.showOKResponseAddDeleteFav();          },
        error=>     { this.warning.errorFavAlert(this.alertCtrl); } 
      );
    }
    else{
      this.daoFirebase.registerFav(this.hero, UserInfo.HERO).then( 
        response=>  { this.showOKResponseAddDeleteFav();          },
        error=>     { this.warning.errorFavAlert(this.alertCtrl); } 
      );
    }
  }

  showOKResponseAddDeleteFav(){
    this.isFav = !this.isFav; 
    this.warning.addDeleteElement(this.toastCtrl, this.isFav, "heroe");
  }

}
