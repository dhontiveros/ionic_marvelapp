import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { UserInfo } from '../../app/models/userinfo';
import { Hero } from '../../app/models/hero';
import { DescriptionHeroPage } from '../description-hero/description-hero';
import { DescriptionComicPage } from '../description-comic/description-comic';
import { DescriptionCreatorPage } from '../description-creator/description-creator';

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {

  favHeroesList:    any;
  favComicsList:    any;
  favCreatorsList:  any;

  showFavHeroes   : boolean;
  showFavComics   : boolean;
  showFavCreators : boolean;

  type: string;

  constructor(  public navCtrl:       NavController, 
                public navParams:     NavParams,
                private authProvider: AuthServiceProvider) 
  {
    
  }

  ionViewDidLoad(){
    this.type = 'heroes';
  }

  ionViewWillEnter(){
    this.loadAttrsView();
  }

  loadAttrsView(){
    this.favHeroesList    = this.authProvider.getUserInfo().getFavList(UserInfo.HERO);
    this.favComicsList    = this.authProvider.getUserInfo().getFavList(UserInfo.COMIC);
    this.favCreatorsList  = this.authProvider.getUserInfo().getFavList(UserInfo.CREATOR);

    this.showFavHeroes    = this.favHeroesList== -1    ? false : true;
    this.showFavComics    = this.favComicsList== -1    ? false : true;
    this.showFavCreators  = this.favCreatorsList== -1  ? false : true;

    console.log("showFavHeroes = "+this.showFavHeroes);
    console.log(this.favHeroesList);
  }


  /*********************************************************************************************************
   * 
   * @param id 
   *********************************************************************************************************/
  getDescription(item: any, typeItem: string){
    switch(typeItem)
    {
      // -----------------------------------------------------------------
      case "hero": 
        this.navCtrl.push(DescriptionHeroPage,{
          id : item.id,
          isFav: true
        });
        break;
      // -----------------------------------------------------------------
      case "comic": 
        this.navCtrl.push(DescriptionComicPage,{
          id : item.id,
          isFav: true
        });
        break;
      // -----------------------------------------------------------------
      case "creator": 
        this.navCtrl.push(DescriptionCreatorPage,{
          id : item.id,
          isFav: true
        });
        break;
      // -----------------------------------------------------------------
    }

    
  }

}
