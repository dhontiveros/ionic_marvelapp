import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { ResetpwdPage } from '../reset-pwd/reset-pwd';
import { DaoFirebaseProvider } from '../../providers/dao-firebase/dao-firebase';
import { UserInfo } from '../../app/models/userinfo';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public loginForm: FormGroup;
  emailChanged:     boolean = false;
  passwordChanged:  boolean = false;
  submitAttempt:    boolean = false;
  loading:          any;

  constructor(  public navCtrl: NavController, 
                public authService: AuthServiceProvider,
                public daoFirebase: DaoFirebaseProvider, 
                public navParams: NavParams, 
                public formBuilder: FormBuilder,
                public alertCtrl: AlertController, 
                public loadingCtrl: LoadingController) 
  {
    this.loginForm = formBuilder.group({
      email:    ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6) ])]
    });
  }

  elementChanged(input){
    let field = input.ngControl.name;
    this[field + "Changed"] = true;
  }

  register(){
    this.navCtrl.push(SignupPage);
  }

  resetPwd(){
    this.navCtrl.push(ResetpwdPage);
  }

  loginUser(){
    this.submitAttempt = true;

    if (!this.loginForm.valid){
      console.log(this.loginForm.value);
    } else {
      this.authService.doLogin(this.loginForm.value.email, this.loginForm.value.password).then( authService => {

        var response = this.daoFirebase.getUserInfoByEmail( this.loginForm.value.email);
        console.log("getUserInfoByEmail");
        console.log(response);
        if( response!=null){
          console.log(response);
          //this.authService.setUserInfo( new UserInfo(response.email, response.name) );
        }
        this.navCtrl.popToRoot();
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });

      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }
  }

}