import { AlertController, ToastController } from 'ionic-angular';

export class WarningAlert{

    showAlert(alertCtrl: AlertController, titleValue: string, bodyValue: string, closeButtonValue: string) {
        const alert = alertCtrl.create({
          title:    titleValue,
          subTitle: bodyValue,
          buttons:  [closeButtonValue]
        });
        alert.present();
    }


    errorResponseAlert(alertCtrl: AlertController){
      this.showAlert(alertCtrl, "Error", "The requested information could not be obtained. Check your internet connection. If the problem persists, contact your administrator", "Close");
      // this.showAlert(alertCtrl, "Error", "No se pudo obtener la información solicitada. Revise su conexión a internet. Si el problema persiste, póngase en contacto con su administrador", "Cerrar");
    }

    errorFavAlert(alertCtrl: AlertController){
      this.showAlert(alertCtrl, "Error", "The item could not be added to the favorites list. If the problem persists, contact the administrator", "Close");
      // this.showAlert(alertCtrl, "Error", "No se pudo agregar el elemento a la lista de favoritos. Si el problema persiste póngase en contacto con el administrador", "Cerrar");
    }


    showToast(toastCtrl: ToastController, msg: string) {
      let toast  = toastCtrl.create({
        message:    msg,
        duration:   3000,
        position:  'bottom'
      });
      toast.present();
    }

    addDeleteElement(toastCtrl: ToastController, addDelete: boolean, item: string){
      /*
      let msg = "Se ha";
      msg += addDelete ? " añadido " : " eliminado ";
      msg += "el "+item+" actual ";
      msg += addDelete ? " a " : " de ";
      msg += "su lista de favoritos";
      */

      // The current hero has been added to the favorites list
     let msg  = "The current ";
     msg      += item+" has been ";
     msg      += addDelete ? "added " : "deleted ";
     msg      += "favorites list";
      this.showToast(toastCtrl, msg);
    }

}