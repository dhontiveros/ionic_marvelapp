import { Md5 } from 'ts-md5/dist/md5';

export class MarvelConfig{

    private maxLimit:   number      = 100;  
    private privateKey: string      = 'd0347172a6aabda572b28c70db609c71b69e5517';
    private publicKey:  string      = '9cacf08247a75aa38a50f7f819aaf229';
    private baseURL:    string      = 'https://gateway.marvel.com/v1/public/';

    private loadTimestamp(){
        return Number( new Date() );
    }

    private loadMD5ByTimeStamp(timestamp){
        return Md5.hashStr( timestamp + this.privateKey + this.publicKey );
    }


    getListHeroes(){
        var timestamp   = this.loadTimestamp();
        var hash        = this.loadMD5ByTimeStamp(timestamp);
        return this.baseURL+'characters?ts='+timestamp+'&orderBy=name&limit='+this.maxLimit+'&apikey='+this.publicKey+'&hash='+hash
    }

    getListComics(){
        var timestamp   = this.loadTimestamp();
        var hash        = this.loadMD5ByTimeStamp(timestamp);
        return this.baseURL+'comics?ts='+timestamp+'&orderBy=title&limit='+this.maxLimit+'&apikey='+this.publicKey+'&hash='+hash
    }

    getDescriptionHeroe(id: number){
        var limit = 20;
        var timestamp   = this.loadTimestamp();
        var hash        = this.loadMD5ByTimeStamp(timestamp);
        return this.baseURL+'characters/'+id+'?ts='+timestamp+'&orderBy=name&limit='+limit+'&apikey='+this.publicKey+'&hash='+hash
    }

    getDescriptionComic(id: number){
        var limit = 20;
        var timestamp   = this.loadTimestamp();
        var hash        = this.loadMD5ByTimeStamp(timestamp);
        return this.baseURL+'comics/'+id+'?ts='+timestamp+'&apikey='+this.publicKey+'&hash='+hash
    }

    getListCreators(){
        var timestamp   = this.loadTimestamp();
        var hash        = this.loadMD5ByTimeStamp(timestamp);
        return this.baseURL+'creators?ts='+timestamp+'&orderBy=lastName&limit='+this.maxLimit+'&apikey='+this.publicKey+'&hash='+hash
    }

    getDescriptionCreator(id: number){
        var limit = 20;
        var timestamp   = this.loadTimestamp();
        var hash        = this.loadMD5ByTimeStamp(timestamp);
        return this.baseURL+'creators/'+id+'?ts='+timestamp+'&apikey='+this.publicKey+'&hash='+hash
    }
}