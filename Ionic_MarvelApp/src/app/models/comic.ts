export class Comic{
    
    id :            number;
    title :         string;
    thumb :         string;
    printPrice:     number;
    description :   string;
    pages:          number;
}