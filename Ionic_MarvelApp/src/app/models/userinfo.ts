export class UserInfo
{
    public static HERO      : number    = 0;
    public static COMIC     : number    = 1;
    public static CREATOR   : number    = 2;

    id:                 string;
    fullName:           string;
    email:              string;
    favHeroesList:      any;
    favComicsList:      any;
    favCreatorsList:    any;

    constructor(_id: string, _email:string, _name: string){
        this.id                 = _id;
        this.email              = _email;
        this.fullName           = _name;

        this.favHeroesList      = -1;
        this.favComicsList      = -1;
        this.favCreatorsList    = -1;
    }

    setEmail(value:string){
        this.email = value;
    }

    setFullName(value:string){
        this.fullName = value;
    }

    getEmail(){
        return this.email;
    }

    getFullName(){
        return this.fullName;
    }

    getId(){
        return this.id;
    }

    getFavList(typeItem: number){
        switch( typeItem ){
            case UserInfo.HERO:     return this.favHeroesList;
            case UserInfo.COMIC:    return this.favComicsList;
            case UserInfo.CREATOR:  return this.favCreatorsList;
            default: return null;
        }
    }

    addFavItem(item: any, typeItem: number){
        switch( typeItem ){
            case UserInfo.HERO:     
                if( this.favHeroesList==-1 ) this.favHeroesList = [];
                this.favHeroesList.push( item );    
                break;

            case UserInfo.COMIC:    
                if( this.favComicsList==-1 ) this.favComicsList = [];
                this.favComicsList.push( item );    
                break;

            case UserInfo.CREATOR:  
                if( this.favCreatorsList==-1 ) this.favCreatorsList = [];
                this.favCreatorsList.push( item );  
                break;
        }
    }


    removeFavItem(item: any, typeItem: number){
        let currentList = null;
        switch( typeItem ){
            case UserInfo.HERO:     currentList = this.favHeroesList; break;
            case UserInfo.COMIC:    currentList = this.favComicsList; break;  
            case UserInfo.CREATOR:  currentList = this.favCreatorsList; break;
        }
        if( currentList!=null && currentList!=-1 ){
            currentList = currentList.filter(obj => obj.id !== item.id);
            if( currentList.length==0 ){
                currentList = -1;
            }
        }
        switch( typeItem ){
            case UserInfo.HERO:     this.favHeroesList      = currentList; break;
            case UserInfo.COMIC:    this.favComicsList      = currentList; break;  
            case UserInfo.CREATOR:  this.favCreatorsList    = currentList; break;
        }
    }


    static getStringFavList(typeItem: number) : string{
        switch( typeItem ){
            case UserInfo.HERO:     return 'favHeroesList';
            case UserInfo.COMIC:    return 'favComicsList';;
            case UserInfo.CREATOR:  return 'favHeroesList';;
        }
    }
}