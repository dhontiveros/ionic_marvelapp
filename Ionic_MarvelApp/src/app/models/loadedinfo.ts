/**
 * Esta clase permite mantener almacenados, en memoria, el listado de los elementos existentes
 * mostrados en cada una de las 3 pantallas principales del sistema:
 */
export class LoadedInfo{
    heroesList:     any;
    comicsList:     any;
    creatorsList:   any;
}