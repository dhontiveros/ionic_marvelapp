import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../config';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { NgxErrorsModule } from '@ultimate/ngxerrors';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MarvelProvider } from '../providers/marvel/marvel';
import { HttpClientModule } from '@angular/common/http';
import { DescriptionHeroPage } from '../pages/description-hero/description-hero';
import { DescriptionComicPage } from '../pages/description-comic/description-comic';
import { DescriptionCreatorPage } from '../pages/description-creator/description-creator';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { ResetpwdPage } from '../pages/reset-pwd/reset-pwd';
import { SingletoninfoProvider } from '../providers/singletoninfo/singletoninfo';
import { DaoFirebaseProvider } from '../providers/dao-firebase/dao-firebase';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DescriptionHeroPage,
    DescriptionComicPage,
    DescriptionCreatorPage,
    LoginPage,
    SignupPage,
    ResetpwdPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    AngularFireModule.initializeApp(firebaseConfig.fire),
    AngularFirestoreModule.enablePersistence(),
    AngularFireDatabaseModule,
    NgxErrorsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DescriptionHeroPage,
    DescriptionComicPage,
    DescriptionCreatorPage,
    LoginPage,
    SignupPage,
    ResetpwdPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MarvelProvider,
    AngularFireAuth,
    AuthServiceProvider,
    SingletoninfoProvider,
    DaoFirebaseProvider,
    InAppBrowser
  ]
})
export class AppModule {}
