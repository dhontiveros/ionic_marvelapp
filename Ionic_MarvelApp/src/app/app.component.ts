import { Component } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { UserInfo } from './models/userinfo';
import { DaoFirebaseProvider } from '../providers/dao-firebase/dao-firebase';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage:any = LoginPage;

  constructor(  private platform: Platform, 
                private statusBar: StatusBar, 
                private splashScreen: SplashScreen,
                private auth: AuthServiceProvider,
                private daoFirebase: DaoFirebaseProvider) 
  {
    this.initializeApp();;
  }

  initializeApp(){
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.auth.afAuth.authState
      .subscribe(
        user => {
          if( user ){
            console.log("app.component.ts");
            console.log(user);
            console.log("email = "+user.email);
            this.daoFirebase.getUserInfoByEmail(user.email).subscribe(
              (res)=>{
                console.log("Cargando nuevo usuario..");
                console.log(res);
                let userInfo = new UserInfo(res[0].id, res[0].email, res[0].fullName);
                userInfo.favHeroesList    = res[0].favHeroesList;
                userInfo.favComicsList    = res[0].favComicsList;
                userInfo.favCreatorsList  = res[0].favCreatorsList;
                this.auth.setUserInfo( userInfo );
                this.rootPage = HomePage;
              },
              (error)=>{
                console.log("ERROR: app.component.ts");
                console.log(error);
                //this.rootPage = HomePage;
              }
            );
          }
          else{
            this.rootPage = LoginPage;
          }
        },
        () => {
          this.rootPage = LoginPage;
        }
      );
  }
}

